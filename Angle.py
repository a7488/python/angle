class Angle(object):
    def __init__(self, deg, min, sec):

        self.deg = deg
        self.min = min
        self.sec = sec
    
    def __add__(self, other):
        
        if  self.sec + other.sec < 60 and self.min + other.min < 60:
            deg = self.deg + other.deg
            min = self.min + other.min
            sec = self.sec + other.sec

        elif   self.sec + other.sec >= 60 and self.min + other.min < 60 :
            sec = self.sec + other.sec - 60 
            min = self.min + other.min + 1
            if  min >= 60:
                deg = self.deg + other.deg + 1
                min = min - 60 
            elif ( min < 60):
                deg = self.deg + other.deg 

        elif   self.sec + other.sec >= 60 and self.min + other.min >= 60 :
            sec = self.sec + other.sec - 60 
            min = self.min + other.min + 1 - 60
            deg = self.deg + other.deg + 1
            
        
        elif   self.sec + other.sec < 60 and self.min + other.min >= 60 :
            sec = self.sec + other.sec 
            min = self.min + other.min - 60
            deg = self.deg + other.deg + 1
    
        return Angle(deg, min, sec)

    def __sub__(self, other):

        if  self.sec >= other.sec and self.min >= other.min :
            deg = self.deg - other.deg
            min = self.min - other.min
            sec = self.sec - other.sec 

        elif  self.sec < other.sec and self.min >= other.min :
           sec = self.sec - other.sec + 60 
           if  self.min == other.min :
                min = 59
                deg = self.deg - other.deg - 1
           elif self.min > other.min :
                min = self.min - other.min - 1
                deg = self.deg - other.deg
        
        elif  self.sec < other.sec  and self.min < other.min :
                sec = self.sec - other.sec + 60
                min = self.min - other.min - 1 + 60
                deg = self.deg - other.deg - 1

        elif   self.sec >=  other.sec and self.min < other.min :
            sec = self.sec - other.sec 
            min = self.min - other.min + 60 
            deg = self.deg - other.deg - 1

        return Angle(deg, min, sec)

    def show(self):
        print("(%1.2f, %1.2f, %1.2f)" % (self.deg, self.min, self.sec))

    def decimial(self):
        return self.deg + self.min/60 + self.sec/3600

    @classmethod 
    def from_decimal(cls, decimal_value):

        deg = int(decimal_value)  
        min = int( (decimal_value - int(decimal_value)) * 60)
        sec = ((decimal_value - int(decimal_value)) * 60 - int( (decimal_value - int(decimal_value)) * 60)) * 60

        return cls(deg, min, sec)

    def __neg__(self):
        deg = - self.deg
        min = self.min
        sec = self.sec
        return  Angle(deg, min, sec)


p1 = Angle(100, 25, 21)
p2 = Angle(60, 20, 20)

print("add")
(p1.__add__(p2)).show()

print("sub")
p = p1.__sub__(p2)
p.show()

print("neg")
p = -p
p.show()

print("decimal")
g = Angle.from_decimal(3.97)
g.show()

# Проверка h = 90° – φ + δ и h = 90° + φ - δ
print("###########################################")
phi = Angle(23, 75, 13)
delta = Angle(45, 58, 14)
ninety = Angle(90, 0, 0)

if  delta.deg > phi.deg :
    print("North of the zenith: ")
    (ninety + phi - delta).show()

elif delta.deg < phi.deg :    
    print("South of the zenith: ") 
    (ninety - phi + delta).show()

